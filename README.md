# QArt-- Mobile number allocation

Setup Server:
	- First of all ensure that latest version npm, node.js and MongoDB is installed.
	- run the command 'npm install' to install all node.js modules.
	- then run command 'node server.js' to start the server
	
Tech Stack:
	- Node.js
	- Express.js
	- MongoDB
	
APIs:
	- GET: 'http://localhost:9555/get_no' will allot a random mobile number between '111-111-1111' and '999-999-9999'.
	- GET: 'http://localhost:9555/get_fno?no=<mobile_no>' will allot a fancy mobile number if available.