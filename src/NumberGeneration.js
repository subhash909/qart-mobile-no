var MongoClient = require('mongodb').MongoClient;

var min_no = 1111111111;
var max_no = 9999999999;    
const url = 'mongodb://localhost:27017';
const dbName = 'qart';
const coll_name = 'mobile_nos';
var db_connection;

// DB connection
var connect_db = ()=>{
    MongoClient.connect(url, function(err, client) {
        if(err) console.log("Error occurred in connecting to DB: ", err);
        else{
            console.log("Connected successfully to server");
            db_connection = client.db(dbName);
        }
    });    
}
connect_db();

// Mobile generation logic

var get_no = (req, res)=>{
    var ran_num = Math.floor((Math.random()*max_no) + min_no);
    check_no_exist(req, res, ran_num, get_no);
}

var get_fancy_no = (req, res)=>{
    var data = req.query || {};
    var no = data.no || "";
    no = Number(no) || 0;
    if(min_no < no && no < max_no){
        check_no_exist(req, res, no);
    }else{
        res.send("Not a valid number.");
    }
}

var check_no_exist = (req, res, t_no, callback)=>{
    if(db_connection){
        const collection = db_connection.collection(coll_name);
        collection.find({m_no: t_no}).toArray((err, results)=>{
            if(err){
                console.error("Error occurred in check_no_exist: ", err);
                res.status(500);
                res.end();
            }else if(!results || !results[0]){
                register_no(t_no);
                res.send({mobile_no: t_no});
            }else{
                callback ? callback(req, res) : res.send("Number already registered.");
            }
        });
    }else{
        res.status(500);
        res.end();
    }
}

var register_no = (t_no)=>{
    if(db_connection){
        const collection = db_connection.collection(coll_name);
        collection.insertOne({m_no: t_no}, (err, result)=>{
            if(err) console.log("Error occurred in register_no: ", err);
        });
    }
}


// Router registration
var register_route = (server)=>{
    
    server.route('/get_no')
    .get(get_no);

    server.route('/get_fno')
    .get(get_fancy_no);

}

var export_obj = {
    register_route: register_route
};

module.exports = export_obj;