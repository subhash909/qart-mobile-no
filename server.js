var http = require('http');
var express = require('express');
var no_generation = require('./src/NumberGeneration');

var server = express();
no_generation.register_route(server);
http.createServer(server).listen(9555, ()=>{
    console.log("Server started on port 9555");
});